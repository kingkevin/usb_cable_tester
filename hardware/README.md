purpose
=======

the name of the device says it all: "USB cable tester".
it will test what kind of USB cable it is by checking which signals on the connectors are inter-connected by the cable.
this helps identifying if a cable can transmit data or just deliver power.
Apple's lightning connector is also supported although it is not strictly a USB connector.

for more details about the product in general, refer to the project's README.
this README will provide details about the hardware design.

design choice
=============

micro-controller
----------------

to check if signals are connected by a cable, only input/output pins (I/Os) are required, as provided by any micro-controller.
since each signal of the each connector will be tested, a large number of I/Os is required.
there are several solutions to get plenty of I/Os:

- Serial-In-Parallel-Out (SIPO) and Parallel-In-Serial-Out (PISO) shift registers
- analog or digital multiplexers
- I/O expanders
- large pin-count micro-controller

I decided to go with a large pin-count micro-controller (MCU) because it's simpler (no need to design in hardware how to combine the signals), requires less components (no additional parts, except some decoupling capacitors), is more flexible (I/Os can be reconfigured), and is not much more expensive than additional devices (even less).
the micro-controller should have USB to be able to re-flash it and get detailed report.
since I am quite familiar with the STM32F1 family, I picked the largest one available: an STM32F103ZC, with 144 pins, including 112 I/Os.

here the number of USB signals required (including 3.0 data lines, shield and individual ground/power):

- host A:          10 (9 for USB 3 + 1 shield)
- host C:          25 (24 with each VCC/GND, + 1 shield)
- device A:        10 (some rare cables are A-A, as for PCIe riser card)
- device B:        10 (9 for USB 3 + 1 shield)
- device mini-B:    6 (4 for USB 2, 1 shield, 1 ID)
- device micro-B:  11 (9 for USB 3, 1 shield, 1 ID)
- device C:        25 (24 with each VCC/GND, 1 shield)
- lightning:        8 (including both differential pairs and ID lines)
- total USB:      105
- MCU USB:          2 (to flash and get detailed report/advanced testing)
- SWD:              2 (to flash and debug the MCU)
- I²C:              2 (to display the cable test result)
- display control:  1 (to power off display at the end)
- total:          112

pins could be shared amongst host and device connectors to save pins, but that would complicate the detection of the used connector and reduce the cable connector combination possibilities.
since there are just enough pins, we can test all signals individually.

display
-------

only two pins are available to show the cable test report on a display.
this is enough for I²C.
there are I²C OLED screens, for example the 0.96 in., with 128x64 pixels.
I was already familiar with this device and had some spare ones.
but they are rather small and can't display a lot of text.

alternatively, 2 lines x 16 characters LCD modules can also be used, called LCM1602.
they are large, easy to read from, ubiquitous and easy to get.
be aware that you need one which can operate at 3.3V.
this is not the case from the most common models which operate at 5.0V.
3.3V models have an additional voltage regulator on the board.
this was also a good opportunity to get familiar with them and learn about the Hitachi HD44780 protocol.

the HD44780 protocol interface requires more than 2 signals to be controlled, but I²C adapter boards exist.
they use a PCF8547 I/O expander, providing 8 lines.
these adapter boards even include a potentiometer to set the LED contrast, and transistor to control the LED backlight.

since I²C is a bus, both screens can be controlled individually without requiring additional signals.
either or both displays can be mounted.

battery
-------

the USB cable tester should be able to be powered over USB, but also to be operated on it's own.
for that, a battery is required.
AA and AAA batteries are ubiquitous and sufficient to power the device, but they require a more complicated voltage step-up converter to boost it from 1.0-1.5 V to 3.3 V.
alternatively, Li-Ion batteries are becoming very common.
the most wide-spread form factor is 18650.
18650 batteries are large, but the device is already lager enough because of the number of connectors and LCD screen already required on the board.
18650 batteries can deliver large currents, but this is not required by this low-power device.
alternatively, a smaller battery pack can be connected using the 2-pin connector next to the battery holder.
a simple low-dropout voltage regulator (LDO) is sufficient to convert the 3.6-4.2 V provided by the battery to the 3.3 V required by the micro-controller.
the same LDO can be used for the 5.0 V provided by the USB port.

a single-cell Li-Ion battery charger is included to recharge the battery.
I wanted to try them since they are not complicated and inexpensive.
a battery protection circuit should be included in the battery since none is present on the board.

the switch next to the USB port is used to select which power source to use to power the micro-controller.
it also allows to completely disconnect the battery from the circuit, by selecting the USB port as power input, preventing even the tiny stand-by current draw.
the USB cable tester will shut down after 5 minutes to save battery.

requirements
============

to be able to generate the outputs you need following software:
- rake: the central script taking care of generating the output files (Makefile is too cumbersome to parse files)
- [QEDA](http://qeda.org/): to generate footprints for the parts
- [QEDA library](https://doc.qeda.org/library/): for the schematic capture
- [pcb-rnd](http://repo.hu/projects/pcb-rnd/): for the board layout
the output generation is automatized.

schematic
=========

library
-------

almost all of the symbols and footprints used in the schematic and board layout are defined in the [QEDA](http://qeda.org/) format and generated for the CAD software.
the `library` folder contains the QEDA parts definitions.

to install QEDA using NPM from the official repository:
~~~
sudo npm install -g qeda
~~~

to install QEDA from the sources:
~~~
git clone https://github.com/qeda/qeda
cd qeda
npm install
sudo npm install --global
~~~

to generate the parts:
~~~
rake library
~~~

this will use the parts definition (.yaml files) in the `library` to generate [gEDA gschem](http://wiki.geda-project.org/geda:gaf)/[Lepton EDA](https://github.com/lepton-eda/lepton-eda) symbols (.sym files) in the `geda/symbols` folder, and [coralEDA pcb-rnd](http://repo.hu/projects/pcb-rnd/) footprints (.lht files) in the `coraleda/subc` folder.

only the QEDA parts in subfolders within `library` come from the [QEDA library](https://doc.qeda.org/library/), but the files are included in this project for simplicity and archiving purposes.
all other parts are custom and written for this project.

schematic
---------

the `usb_cable_tester.sch` file is the schematic source file.
it has been drawn using [Lepton EDA](https://github.com/lepton-eda/lepton-eda).

it uses standard symbols, and the ones in the `geda/symbols/` folder.
only `bordered-A1.sym` and `title.sym` are hand drawn.
all other symbols are generated by QEDA as described above.

to export the netlist (in tEDAx format):
~~~
rake netlist
~~~

to export as pdf:
~~~
rake print
~~~

BOM
---

to export the bill of material (as CSV):
~~~
rake bom
~~~

board
=====

the `usb_cable_test.lht` file is the board layout source file.
it has been drawn using  [coralEDA pcb-rnd](http://repo.hu/projects/pcb-rnd/).

it uses the symbols from the `coraleda/subc/` folder.
`oshw_logo.lht` is just the Open Source Hardware Logo.
it been generated from https://oshwlogo.cuvoodoo.info/.
all other symbols are generated by QEDA as described above.

to export gerber files for PCB manufacturer (and photo preview + overview document):
~~~
rake fabrication
~~~

fabrication
===========

the board has components on both sides.
it is a mix of through hole components (USB connectors, displays, batteries, switches) and surface mount components (the rest, mainly the micro-controller and power related parts).
the connectors need to be through hole components to withstand the mechanical load when plugin in and removing the cables.

use board with a thickness of maximum 1.2 mm since the USB-C leads are only 1.3 mm long.
