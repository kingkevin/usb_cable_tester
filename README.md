purpose
=======

the name of the device says it all: "USB cable tester".
it will test what kind of USB cable it is by checking which signals on the connectors are inter-connected by the cable.
this helps identifying if a cable can transmit data or just deliver power.
Apple's lightning connector is also supported although it is not strictly a USB connector.

background
----------

micro-USB (USB micro-B) cables come with each new gadget to power it, or recharge its battery.
these cable are not actual USB cables (which can transfer USB data), but only deliver power.
the USB connectors are just used because it is convenient, and an ubiquitous power source.
but you don't want to confuse them with actual USB cables supporting data transfer.
sadly there are often no visible differences between a USB data cable and one just to deliver power.
the USB cable tester is a tool to identify the USB cables.
afterwards don't forget to label them afterwards to not get tricked in the future and stop wasting debugging time.

since I would built a device to test USB cables, why stop at just testing if it has data lines.
it can also test if the cable is shielded, supports USB 3.x, and even other USB connectors.
this becomes even more interesting for USB-C cables since they have plenty of signals, some optional, and this is not identifiable just by looking at the cable.

connectors
----------

the USB cable tester has the following connectors:
- 2x USB type-A, supporting USB 3.x
- 2x USB type-C, with all 24 pins
- 1x USB type-B, supporting USB 3.x
- 1x USB mini-B, USB 2.0 only
- 1x USB micro-B, supporting USB 3.x
- 1x Apple lightning

these are the most commonly used USB connectors.
the Apple lightning socket has been added because it is also a common charging connector, and there were enough free pins to also test this.
there are two USB type-A connectors because some rare devices also use it (for example PCIe extension boards), although this should be a host-side connector.
some Y-cables also use a second type-A host connector for devices requiring additional power.
any combination of the connectors and signals can be tested (a list of know cables is built in the firmware).

usage
-----

- plug all ends of the USB cable to test in the corresponding sockets
- choose if the USB cable tester should be powered through its USB port or the battery using the switch
- press on the RESET/START button
- the result of the cable test will be displayed on the LCD and/or OLED screen

if you connect the USB cable tester over USB (using the dedicated micro-B connector) to a computer, it will appear as USB device with a serial port.
you can connect to it using a terminal (the baud rate does not matter) and have a console for more detailed tests and results.
this is useful for cables with could not be identified, often because they are faulty.
more tests are available on the serial prompt to identify the issues with the cable.
just type "help" to get the available commands.

limitations
-----------

the USB cable tester only tests if the signals are connected.
it does not test:

- the resistance of the cable (useful for cables used to delivery high currents)
- if the shield is actually shielding the data cables or is just a wire (the impedance would need to be measured, which is complicated)
- if the data line pairs are twisted (the impedance would need to be measured, which is complicated)

I did not include the mini-A, micro-A, mini-AB, and micro-AB connectors since they are quite uncommon.

firmware
========

binary
------

ready to flash firmware binaries are in the `release` folder

source code
-----------

the firmware source code for this device is in the `firmware` folder.
if you got this project using git, you need also check out this submodule using `git submodule init`.
the source code in under GPL version 3 license.
for more details check the `README` file on the `firmware` folder.
this will also provide informations for firmware development.

hardware
========

production
----------

the pdf schematic, board layer gerbers, and various other output files are in the `release` folder.

design files
------------

the source design files are in the `hardware` folder.
they are under the CERN Open Hardware Licence version 1.2.
for more information check the `README` file on the `hardware` folder.

